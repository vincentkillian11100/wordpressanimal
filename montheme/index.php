<?php get_header();
// the query
the_a_z_listing($query);
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>

<?php if ( $a_z_query->have_posts() ) : ?>
<div class="">
    <!-- the loop -->
    <?php while ( $a_z_query->have_posts() ) : $a_z_query->the_post(); ?>
            <?php the_post_thumbnail('post-thumbnail', ['class' => 'img', 'alt' => 'image'] ) ?>
                <h2 class="card-title"><?php the_title(); ?></h2>
                <a href="<?php the_permalink()?>" class="lien">Détail de l'article </a>
    <?php endwhile; ?>
    <!-- end of the loop -->
    <?php wp_reset_postdata(); ?>
</div>L
<?php else : ?>
<p><?php _e( 'Aucun article ne correspond.' ); ?></p>
<?php endif; ?>

<?php get_footer() ?>