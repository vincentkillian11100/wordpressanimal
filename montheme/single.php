<?php get_header() ?>
<main>
    <div class="container">
        <div class="containerRight">
            <h1 id="titreAn"><?php the_title(); ?></h1>
            <?php if (get_field('taille')) : ?>
                <h2><?php the_title(); ?> fait en moyenne <?php the_field('taille'); ?> toises</h2>
            <?php endif; ?>
            <h2 id="description"><?php the_field('content'); ?></h2>
        </div>
        <div class="containerLeft">
            <?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top', 'alt' => 'image']) ?>
            <?php
$featured_posts = get_field('animaux');
if( $featured_posts ): ?>
    <ul>
    <?php foreach( $featured_posts as $post ): 

        // Setup this post for WP functions (variable must be named $post).
        setup_postdata($post); ?>
        <li>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </li>
    <?php endforeach; ?>
    </ul>
    <?php 
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>
<?php endif; ?>
        </div>
    </div>
</main>
<?php get_footer() ?>
